<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model{


    protected $table = 'settings';
    protected $fillable = ['option','slug','value'];
    protected $dates = ['created_at', 'updated_at'];

    public static function get($slug){

        $_setting_item = Setting::where('slug', $slug)->first();


        return $_setting_item ? $_setting_item->value : null;

    }
}
