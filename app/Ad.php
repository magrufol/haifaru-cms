<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Ad extends Model{

    protected $fillable = ['title', 'link', 'note', 'image'];

    public function posts(){

        return $this->belongsToMany(
            Post::class,
            'post_ads',
            'ad_id',
            'post_id'
        );

    }


    public function getImage($thumb = false){

        if($this->image == null){
            return '/img/' . self::IMG_PLACEHOLDER;
        }

        $_path = $thumb ? 'storage/uploads/thumbs/' : 'storage/uploads/';

        return  asset($_path . $this->image) ;

    }


    public static function add($fields){

        $post = new static;
        $post->fill($fields);
        $post->save();

        return $post;

    }

}
