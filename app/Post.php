<?php

namespace App;

use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;


class Post extends Model{

    use Sluggable;

    protected $fillable = [
        'title',
        'date',
        'description',
        'fb_post_link',
        'content',
        'image'
    ];

    protected $guarded = [];


    const IS_DRAFT = 0;
    const IS_PUBLIC = 1;
    const IMG_PLACEHOLDER = 'no-image.jpg';

    public function category(){

        return $this->belongsTo(Category::class);

    }

    public function author(){

        return $this->belongsTo(User::class, 'user_id');

    }

    public function comments(){

        return $this->hasMany(Comment::class);

    }

    public function tags(){

        return $this->belongsToMany(
            Tag::class,
            'post_tags',
            'post_id',
            'tag_id'
        );

    }

    public function ads(){

        return $this->belongsToMany(
            Ad::class,
            'post_ads',
            'post_id',
            'ad_id'
        )->withPivot('type');

    }


    public function sluggable(){

        return [
            'slug' => [
                'source' => 'title'
            ]
        ];

    }

    public static function add($fields){

        $post = new static;
        $post->fill($fields);


        $post->user_id = Auth::user()->id;
        $post->save();

        return $post;

    }

    public function edit($fields){

        $this->fill($fields);
        $this->save();

    }

    public function remove(){

        $this->removeImage();
        $this->delete();

    }

    public function uploadImage($image){

        if($image == null) { return; }

        $this->removeImage();
        $filename = Str::random(10) . '.' . $image->extension();
        //$image->storeAs('public/uploads', $filename);

        Storage::putFileAs('', $image, $filename);

        $this->image = $filename;
        $this->save();

    }

    public function downloadImage($url){

        if($url == null) { return; }

        $this->removeImage();
        $contents = file_get_contents($url);
        $extension = pathinfo(parse_url($url, PHP_URL_PATH), PATHINFO_EXTENSION);
        $filename = Str::random(10) . '.' . $extension;

        Storage::put($filename, $contents);
        //$res = Storage::disk('s3')->put($filename, $contents);

        $image = Image::make($contents)
            ->fit(config('lfm.thumb_img_width', 200), config('lfm.thumb_img_height', 200));

        Storage::put('/thumbs/' . $filename, $image->stream()->detach());



        $this->image = $filename;
        $this->save();

    }

    public function removeImage(){
        if($this->image != null){
            Storage::delete('public/uploads/' . $this->image);
        }
    }

    public function setCategory($id){
        if($id == null)
            return;

        $this->category_id = $id;
        $this->save();

    }

    public function setAd($ids){
        if($ids == null)
            return;

        $this->ads()->sync($ids);

    }

    public function setTags($ids){
        if($ids == null)
            return;

        $this->tags()->sync($ids);
    }

    public function setAds($ids){
        if($ids == null)
            return;

        $this->ads()->sync($ids, false);
    }

    public function setDraft(){

        $this->status = POST::IS_DRAFT;
        $this->save();

    }

    public function setPublic(){

        $this->status = POST::IS_PUBLIC;
        $this->save();

    }

    public function toggleStatus($value){


        if($value == null){
            return $this->setDraft();
        }

        return $this->setPublic();

    }


    public function setFeatured(){

        $this->is_featured = 1;
        $this->save();

    }

    public function setStandart(){

        $this->is_featured = 0;
        $this->save();

    }

    public function toggleFeatured($value){

        if($value == null){
            return $this->setStandart();
        }

        return $this->setFeatured();

    }

    public function getImage($thumb = false){

        if($this->image == null){
            return '/img/' . self::IMG_PLACEHOLDER;
        }

        $_path = $thumb ? 'storage/uploads/thumbs/' : 'storage/uploads/';

        return  asset($_path . $this->image) ;

    }

    public function setDateAttribute($value){

        $date = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
        $this->attributes['date'] = $date;

    }

    public function getDateAttribute($value){

        return Carbon::createFromFormat('Y-m-d', $value)->format('d/m/Y');

    }

    public function getCategoryTitle(){

        return ($this->category != null) ? $this->category->title : 'Нет категории';

    }

    public function getTagTitles(){

        return (!$this->tags->isEmpty())
            ? implode(', ', $this->tags->pluck('title')->all())
            : 'Нет тегов';

    }

    public function getCategoryID(){
        return $this->category !== null ? $this->category->id : null;
    }

    public function getDate(){

        return Carbon::createFromFormat('d/m/Y', $this->date)->format('F d, y');

    }

    public function hasPrevious(){

        return self::where('id', '<', $this->id)->max('id');

    }

    public function getPrevious(){

        $post = $this->hasPrevious(); //id

        return self::find($post);

    }

    public function hasNext(){

        return self::where('id', '>', $this->id)->min('id');

    }

    public function getNext(){

        $post = $this->hasNext(); //id

        return self::find($post);

    }

    public function related(){
        return self::all()->except($this->id);
    }

    public function hasCategory(){
        return $this->category !== null;
    }

    public static function getPopularPosts(){
        return self::orderBy('views', 'desc')->take(3)->get();
    }

    public static function getFeaturedPosts(){
        return self::where('is_featured', 1)->take(3)->get();
    }

    public static function getRecentPosts(){
        return self::orderBy('date', 'desc')->take(4)->get();
    }

    public function getComments(){

        return $this->comments()->where('status', 1)->get();

    }
}
