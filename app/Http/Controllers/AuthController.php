<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Illuminate\Http\Request;

class AuthController extends Controller{

    public function registerForm(){
        return view('pages.register');
    }

    public function register(Request $request){

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required'
        ]);

        $user = User::add($request->all());
        $user->generatePassword($request->get('password'));

        return redirect('/login');

    }


    public function loginForm(){
        return view('auth.login');
    }

    public function login(Request $request){

        $this->validate($request,[
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $remember_token = ($request->has('remember'));

        if(Auth::attempt([
            'email' => $request->get('email'),
            'password' => $request->get('password')
        ], $remember_token)){
            return redirect()->route('dashboard');
        }

        return redirect()->back()->with('status', 'Неправильный логин и пароль ');

    }

    public function logout(){
        Auth::logout();
        return redirect('/login');
    }
}
