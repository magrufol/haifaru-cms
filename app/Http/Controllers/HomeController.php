<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;
use App\Post;
use App\Tag;
use App\Category;
use GuzzleHttp\Client;
use UniSharp\LaravelFilemanager\Controllers\UploadController;


class HomeController extends Controller
{

    private $paginate_count = 15;

    public function index(){


        $latest_posts = Post::orderBy('date')->take(3)->get();

        $categories = json_decode(Setting::get('_home_categories'));
        $categories_section = [];


        if($categories){
            foreach ($categories as $cat){

                $posts = Post::where('category_id', $cat)->orderBy('date')->take(6)->get();

                if($posts){
                    $categories_section[] = [
                        'title' => Category::findOrFail($cat)->title,
                        'posts' => $posts,
                ];
                }
            }
        }


        return view('pages.index', [
            'latest' => $latest_posts,
            'categories_section' => $categories_section
        ])->with([
            'view_classes' => '',
            'show_share_bar' => false
        ]);
    }

    public function showDate($date){

        $posts = Post::where('date', $date)->get();

        dd($posts);

    }

    public function show($slug){

        $post = Post::where('slug', $slug)->firstOrFail();
        $post_ads = [
            'content' => [],
            'sidebar' => []
        ];

        foreach ($post->ads as $ad){
            $post_ads[$ad->pivot->type][] = $ad;
        }

        $latest_posts = Post::orderBy('created_at', 'desc')->take(3)->get();


        return view('pages.show', ['post' => $post])
            ->with([
                'view_classes'   => 'single-post-page',
                'show_share_bar' => true,
                'fb_post_link'   => $post->fb_post_link,
                'post_ads'       => $post_ads,
                'latest_posts'   => $latest_posts
            ]);
    }

    public function tag($slug){

        $tag = Tag::where('slug', $slug)->firstOrFail();

        $posts = $tag->posts()->paginate(2);

        return view('pages.list', ['posts' => $posts]);

    }

    public function category($slug){

        $category = Category::where('slug', $slug)->firstOrFail();


        $posts = $category->posts()->paginate($this->paginate_count);

        return view('pages.category', ['posts' => $posts, 'title' => $category->title])
            ->with([
            'view_classes' => 'single-post-page',
            'show_share_bar' => false
        ]);

    }

    public function updateFromWP(){
        //$client = new Client(['base_uri' => 'http://haifaru.co.il/wp-json/lar_exp/lar']);

        $client = new Client();

        $response = $client->request('GET', 'http://haifaru.co.il/wp-json/lar_exp/lar', [
            'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
        ]);

        $posts = json_decode($response->getBody()->getContents());

        //dd($posts);

        foreach ($posts as $p){
            if(property_exists($p, 'title')){
                $post = Post::add([
                    'title' => $p->title,
                    'content' => $p->content,
                    'slug' => $p->slug,
                    'created_at' => $p->date,
                    'date' => $p->date
                ]);

                $post->setCategory($p->category);
                $post->downloadImage($p->thumb);
                $post->setPublic();
            }
        }

    }
}


