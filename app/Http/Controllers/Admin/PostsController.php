<?php

namespace App\Http\Controllers\Admin;

use App\Ad;
use App\Category;
use App\Http\Controllers\Controller;
use App\Post;
use App\Tag;
use Illuminate\Http\Request;
use function Sodium\add;

class PostsController extends Controller
{

    public function index(){

        $posts = Post::orderBy('created_at', 'desc')->get();

        return view('admin.posts.index', ['posts' => $posts]);

    }

    public function create(){

        $tags = Tag::pluck('title', 'id')->all();
        $categories = Category::pluck('title', 'id')->all();
        $ads = Ad::all();

        return view('admin.posts.create', [
            'tags'       => $tags,
            'categories' => $categories,
            'ads'        => $ads
        ]);

    }


    public function store(Request $request){

        $this->validate($request,[
           'title' => 'required',
           'content' => 'required',
           'date' => 'required',
           'image' => 'required'
        ]);

        $post = Post::add($request->all());
        $this->_updatePost($post, $request);
        return redirect()->route('posts.index');

    }



    public function edit($id){

        $post = Post::find($id);
        $tags = Tag::pluck('title', 'id')->all();
        $categories = Category::pluck('title', 'id')->all();
        $selectedTags = $post->tags->pluck('id')->all();
        $selectedAds = collect($post->ads->all());
        $ads = Ad::all()->except($selectedAds->pluck('id')->toArray());

        return view('admin.posts.edit', [
            'tags' => $tags,
            'categories' => $categories,
            'selectedTags' => $selectedTags,
            'post' => $post,
            'ads' => $ads,
            'selectedAds' => $selectedAds
        ]);

    }


    public function update(Request $request, $id){


        $this->validate($request,[
            'title' => 'required',
            'content' => 'required',
            'date' => 'required',
            'image' => 'required'
        ]);

        $post = Post::find($id);
        $post->edit($request->all());
        $this->_updatePost($post, $request);

        return redirect()->route('posts.index');

    }

    public function destroy($id){
        Post::find($id)->remove();
        return redirect()->route('posts.index');
    }

    private function _updatePost($post, $request){
        $post->setCategory($request->get('category_id'));
        $post->setTags($request->get('tags'));
        $post->toggleStatus($request->get('status'));
        $post->toggleFeatured($request->get('is_featured'));

        //dd($request->get('post_ads_content'), $request->get('post_ads_right'));

        if($request->get('post_ads_content')){
            $_ads_content = [];
            foreach ($request->get('post_ads_content') as $c){
                $_ads_content[$c] = ['type' => 'content'];
            }

            $post->setAds($_ads_content);
        }

        if($request->get('post_ads_right')){
            $_ads_content = [];
            foreach ($request->get('post_ads_right') as $c){
                $_ads_content[$c] = ['type' => 'sidebar'];
            }

            $post->setAds($_ads_content);
        }
    }
}
