<?php

namespace App\Http\Controllers\Admin;

use App\Ad;
use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Cache\Factory;
use Illuminate\Http\Request;
use App\Setting;
use Session;
use function Sodium\add;

class SettingController extends Controller
{

    public function index()
    {
        $settings = Setting::all();
        return view('settings.index', compact('settings'));
    }

    public function homePageSettings(){

        $categories = Category::all();
        $home_popular = Setting::get('_home_popular');
        $home_categories = json_decode(Setting::get('_home_categories'));


        return view('settings.home_page',[
            'is_popular' => $home_popular,
            'categories' => $categories,
            '_home_categories' => $home_categories
        ]);
    }

    public function advSettings(){

        $globalAdv = json_decode(Setting::get('global-adv'), true);
        $selectedAdsRight = Ad::findMany($globalAdv['right']);
        $selectedAdsContent = Ad::findMany($globalAdv['content']);

        $allSelected = $selectedAdsRight->merge($selectedAdsContent);
        $ads = Ad::all()->except($allSelected->pluck('id')->toArray());



        return view('settings.adv',[
            'ads' => $ads,
            'selectedAdsRight' => $selectedAdsRight,
            'selectedAdsContent' => $selectedAdsContent,
        ]);
    }

    public function storeHomeSettings(Request $request, Factory $cache){

        $settings = [
            [
                'slug' => '_home_popular',
                'title' => 'Показывать популярные статьи',
                'value' =>$request->input('_home_popular', 'off')
            ],
            [
                'slug' => '_home_categories',
                'title' => 'Категории на главной странице',
                'value' =>$request->input('_home_categories')
            ]
        ];
        $update_result = true;

        foreach ($settings as $s){
            $update_result = $this->updateSetting($s['slug'], $s['title'], $s['value']);
        }

        $cache->forget('settings');

        if($update_result !== false){
            Session::flash('message', 'Настройки сохранены');
            Session::flash('alert-class', 'alert-success');
        }

        return redirect()->route('settings.home');
    }

    public function storeAdvSettings(Request $request, Factory $cache){

        $globalAdv = [
            'right' => [],
            'content' => []
        ];

        if($request->get('post_ads_content')){
            foreach ($request->get('post_ads_content') as $c){
                $globalAdv['content'][] = $c;
            }
        }

        if($request->get('post_ads_right')){
            foreach ($request->get('post_ads_right') as $c){
                $globalAdv['right'][] = $c;
            }
        }

        $this->updateSetting('global-adv', 'Глобальные рекламные блоки', json_encode($globalAdv));

        return redirect()->route('settings.adv');
    }

//    public function edit($id)
//    {
//        $setting   = Setting::findOrFail($id);
//        return view('edit', compact('setting'));
//    }

//    public function update(Request $request, $id, Factory $cache)
//    {
//
//        $input = $request->all();
//
//        $messages = [
//            'option.required' => 'Please fill setting name.',
//            'slug.required' => 'Please fill setting slug.',
//            'value.required' => 'Please fill setting value.',
//        ];
//
//        $this->validate($request,[
//            'option' => 'required|max:255',
//            'slug' => 'required|max:255',
//            'value' => 'required',
//        ], $messages);
//
//        $data = Setting::findOrFail($id);
//        $data->fill($input)->save();
//
//        /* Important ********** */
//        $cache->forget('settings');
//        /* //Important ********** */
//
//        $request->session()->flash('alert-success', 'Setting updated successfully!');
//        return redirect('/settings');
//    }



    private function updateSetting($slug, $title, $value){
        $data = Setting::firstOrCreate(
            ['slug' => $slug],
            ['option' => $title, 'value' => '{}']
        );

        return $data->fill([
            'option' => $title,
            'slug' => $slug,
            'value' => $value
        ])->save();
    }

}
