<?php

namespace App\Http\Controllers\Admin;

use App\Ad;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class AdsController extends Controller{

    public function index(){

        $ads = Ad::all();
        return view('admin.ads.index', ['ads' => $ads]);

    }

    public function store(Request $request){

        $this->validate($request, [
            'title' => 'required',
            'image' => 'required'
        ]);
        //dd($request->file('image'));
        $ad = Ad::create($request->all());
        //$ad->uploadImage($request->file('image'));
        return redirect()->route('ads.index');

    }

    public function create(){

        return view('admin.ads.create');

    }

}
