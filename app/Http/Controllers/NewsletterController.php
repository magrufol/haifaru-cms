<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subscription;

class NewsletterController extends Controller{

    function storeRequest(Request $request){

        $respond = [
          'status' => 'fail',
          'msg' => 'Что то пошло не так, попробуйте еще раз'
        ];

        $sub = Subscription::where('email',  $request->input('email'))->first();

        if($sub === null){
            $this->validate($request,[
                'email' => 'required|email',
            ]);

            $new_sub = new Subscription;
            $new_sub->add($request->input('email'));

            if($new_sub){
                $respond['status'] = 'success';
                $respond['msg'] = 'Спасибо, ждите нашей рассылки.';
            }
        }else{
            $respond['status'] = 'error';
            $respond['msg'] = 'Вы уже подписались. Ждите новостей.';
        }


        return $respond;
    }
}
