<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use App\Category;
use App\Comment;
use Illuminate\Support\ServiceProvider;
use App\Post;

class AppServiceProvider extends ServiceProvider
{
    public function register(){

    }

    public function boot(){

        view()->composer('layouts.site', function($view){
            $view->with([
                'categories' => Category::all(),
            ]);
        });

    }
}
