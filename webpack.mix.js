const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const admin_res_path = 'resources/admin/';
const front_res_path = 'resources/front/';


//Front
mix.sass('resources/front/sass/app.scss', 'public/css/front/');

mix.scripts([
    front_res_path + 'js/jquery-1.12.js',
    front_res_path + 'js/popper.min.js',
    front_res_path + 'js/jquery-ui.js',
    front_res_path + 'js/bootstrap.js',
    front_res_path + 'js/jquery.ui.datepicker-he.js',
    front_res_path + 'js/app.js',
], 'public/js/front/app.js');

//Admin
mix.styles([
    admin_res_path + 'css/adminlte.css',
    admin_res_path + 'plugins/select2/css/select2.min.css',
    admin_res_path + 'plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css',
    admin_res_path + 'plugins/plugins/fontawesome-free/css/all.min.css',
    admin_res_path + 'plugins/drag-and-drop/dragula/dragula.css',
    admin_res_path + 'plugins/drag-and-drop/dragula/example.css',
    admin_res_path + 'plugins/icheck-bootstrap/icheck-bootstrap.min.css',
], 'public/css/admin/admin.css');

mix.copy(admin_res_path + 'plugins/icheck-bootstrap/icheck-bootstrap.min.css', 'public/css/admin');
mix.copy(admin_res_path + 'plugins/fontawesome-free/css/all.min.css', 'public/css/admin');


mix.scripts([
    admin_res_path + 'plugins/jquery/jquery.min.js',
    admin_res_path + 'plugins/bootstrap/js/bootstrap.bundle.min.js',
    admin_res_path + 'plugins/datatables/jquery.dataTables.min.js',
    admin_res_path + 'plugins/datatables-bs4/js/dataTables.bootstrap4.min.js',
    admin_res_path + 'plugins/datatables-responsive/js/dataTables.responsive.min.js',
    admin_res_path + 'plugins/datatables-responsive/js/responsive.bootstrap4.min.js',
    admin_res_path + 'plugins/moment/moment.min.js',
    admin_res_path + 'plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js',
    admin_res_path + 'plugins/select2/js/select2.full.min.js',
    admin_res_path + 'plugins/daterangepicker/daterangepicker.js',
    admin_res_path + 'plugins/bootstrap-switch/js/bootstrap-switch.min.js',
    admin_res_path + 'plugins/drag-and-drop/dragula/dragula.min.js',
    admin_res_path + 'plugins/drag-and-drop/dragula/custom-dragula.js',
    admin_res_path + 'js/adminlte.min.js',
    admin_res_path + 'js/setup.js',
], 'public/js/admin/admin.js');
