function preview_image(event) {
    var reader = new FileReader();
    reader.onload = function()
    {
        var output = document.getElementById('postImage');
        output.src = reader.result;
    }
    reader.readAsDataURL(event.target.files[0]);
}

$(function () {
    $("#posts-list").DataTable({
        "responsive": true,
        "autoWidth": false,
        language: {
            url: '/js/admin/data-table-ru.json'
        }
    });
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
    });

    $('#ads_post').DataTable({
        "responsive": true,
        "autoWidth": false,
    });

    $('.ad-type').change(function (e) {
        var _id = $(this).data('id');

        $('#' + _id).prop('checked', false);
    });

    $('.select2bs4').select2({
        theme: 'bootstrap4'
    });



    $('#postdate').datetimepicker({
        format: 'DD/MM/YYYY',
        date: $('#postdate').data('value')
    });

    $("input[data-bootstrap-switch]").each(function(){
        $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });
});
