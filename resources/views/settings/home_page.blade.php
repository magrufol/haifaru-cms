@extends('layouts.admin')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Настройки главной страницы</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Список публикаций</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content">
            {{ Form::open([
                'route' => ['settings.store.home'],
                 'method' => 'put'
              ])
            }}
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        @if(Session::has('message'))
                            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title"></h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="exampleInputFile">Показывать популярные статьи</label>
                                    <div class="input-group">
                                        <input type="checkbox" name="_home_popular" {{ $is_popular == 'on' ? 'checked' : '' }} data-bootstrap-switch data-off-color="danger" data-on-color="success">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Категории на главной странице</h3>
                            </div>
                            <div class="card-body">
                                <div id='left-defaults' class='dragula'>

                                    @foreach($_home_categories as $key => $cat_line)
                                        <div class="media  d-md-flex d-block text-sm-left text-center">
                                            <div class="media-body">
                                                <div class="d-xl-flex d-block justify-content-between">
                                                    <div class="d-flex w-75">
                                                        <select class="form-control home_cat"  id="_home_cat_{{ $key }}">
                                                            @foreach($categories as $cat)
                                                                <option value="{{ $cat->id }}" {{ $cat_line == $cat->id ? 'selected' : '' }}>{{ $cat->title . '(' . $cat->postsCount() .')' }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="d-flex justify-content-center align-items-center">
                                                        <i class="fas fa-arrows-alt"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                        <input type="hidden" id="_home_categories"  name="_home_categories">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="card card-primary">
                            <div class="card-body">
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </section>
    </div>
@endsection
