@extends('layouts.admin')

@section('content')
    <div class="content-wrapper">
        <section class="content">
            {{ Form::open([
                'route' => ['settings.adv.home'],
                 'files' => true,
              ])
            }}
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        @include('admin.errors')
                    </div>
                    <div class="col-md-12">
                        <div class="card card-warning">
                            <div class="card-header">
                                <h3 class="card-title">Рекламные блоки</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table id="ads_post" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Картинка</th>
                                                <th>Заголовок</th>
                                                <th>Ссылка</th>
                                                <th>Действия</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($selectedAdsRight as $ad)
                                                <tr>
                                                    <td>{{ $ad->id }}</td>
                                                    <td>
                                                        <img src="{{ $ad->getImage(true) }}" width="200px"
                                                             alt="{{ $ad->title }}" class="img-thumbnail">
                                                    </td>
                                                    <td>{{ $ad->title }}</td>
                                                    <td>
                                                        <a href="{{ $ad->link }}">Ссылка</a>
                                                    </td>
                                                    <td>
                                                        <div class="icheck-success d-inline for-{{ $ad->id }}">
                                                            <input type="checkbox"  name="post_ads_content[]"
                                                                   value="{{ $ad->id }}" class="ad-type"  data-id="ad-{{ $ad->id }}-right" id="ad-{{ $ad->id }}-content">
                                                            <label for="ad-{{ $ad->id }}-content">
                                                                После контента
                                                            </label>
                                                        </div>
                                                        <br>
                                                        <br>
                                                        <div class="icheck-success d-inline for-{{ $ad->id }}">
                                                            <input type="checkbox" checked name="post_ads_right[]"
                                                                   value="{{ $ad->id }}" class="ad-type" data-id="ad-{{ $ad->id }}-content" id="ad-{{ $ad->id }}-right">
                                                            <label for="ad-{{ $ad->id }}-right">
                                                                Справа
                                                            </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            @foreach($selectedAdsContent as $ad)
                                                <tr>
                                                    <td>{{ $ad->id }}</td>
                                                    <td>
                                                        <img src="{{ $ad->getImage(true) }}" width="200px"
                                                             alt="{{ $ad->title }}" class="img-thumbnail">
                                                    </td>
                                                    <td>{{ $ad->title }}</td>
                                                    <td>
                                                        <a href="{{ $ad->link }}">Ссылка</a>
                                                    </td>
                                                    <td>
                                                        <div class="icheck-success d-inline for-{{ $ad->id }}">
                                                            <input type="checkbox" checked name="post_ads_content[]"
                                                                   value="{{ $ad->id }}" class="ad-type"  data-id="ad-{{ $ad->id }}-right" id="ad-{{ $ad->id }}-content">
                                                            <label for="ad-{{ $ad->id }}-content">
                                                                После контента
                                                            </label>
                                                        </div>
                                                        <br>
                                                        <br>
                                                        <div class="icheck-success d-inline for-{{ $ad->id }}">
                                                            <input type="checkbox"  name="post_ads_right[]"
                                                                   value="{{ $ad->id }}" class="ad-type" data-id="ad-{{ $ad->id }}-content" id="ad-{{ $ad->id }}-right">
                                                            <label for="ad-{{ $ad->id }}-right">
                                                                Справа
                                                            </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            @foreach($ads as $ad)
                                                <tr>
                                                    <td>{{ $ad->id }}</td>
                                                    <td>
                                                        <img src="{{ $ad->getImage(true) }}" width="200px"
                                                             alt="{{ $ad->title }}" class="img-thumbnail">
                                                    </td>
                                                    <td>{{ $ad->title }}</td>
                                                    <td>
                                                        <a href="{{ $ad->link }}">Ссылка</a>
                                                    </td>
                                                    <td>
                                                        <div class="icheck-success d-inline for-{{ $ad->id }}">
                                                            <input type="checkbox"  name="post_ads_content[]"
                                                                   value="{{ $ad->id }}" class="ad-type"  data-id="ad-{{ $ad->id }}-right" id="ad-{{ $ad->id }}-content">
                                                            <label for="ad-{{ $ad->id }}-content">
                                                                После контента
                                                            </label>
                                                        </div>
                                                        <br>
                                                        <br>
                                                        <div class="icheck-success d-inline for-{{ $ad->id }}">
                                                            <input type="checkbox"  name="post_ads_right[]"
                                                                   value="{{ $ad->id }}" class="ad-type" data-id="ad-{{ $ad->id }}-content" id="ad-{{ $ad->id }}-right">
                                                            <label for="ad-{{ $ad->id }}-right">
                                                                Справа
                                                            </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="card card-primary">
                            <div class="card-body">
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                            </div>
                        </div>
                    </div>
                </div>
            {{ Form::close() }}
        </section>
    </div>
@endsection
