@extends('layouts.site')

@section('content')

    <section class="single-post-section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <h1 class="post-title">
                        {{ $post->title }}
                    </h1>
                    <div class="single-post-date">{{ $post->date }}</div>
                </div>
                <div class="col-lg-8 col-md-8">
                    <div class="single-post-image">
                        <img src="{{ $post->getImage() }}"
                             alt="{{ $post->title }}"
                             class="img-fluid"
                             title="{{ $post->title }}">
                        <p class="image-title">
                            {{ $post->title }}
                        </p>
                    </div>
                    <article class="single-post-content">
                        {!! $post->content !!}
                    </article>
                    <div class="link-images-content mt-3">
                        @forelse($post_ads['content'] as $s_ad)
                            <a href="{{ $s_ad->link }}" target="_blank" class="mb-1">
                                <img src="{{ $s_ad->getImage() }}" class="img-fluid" alt="Превью">
                            </a>
                        @empty
                        @endforelse
                    </div>
                    @php
                    $tags = $post->tags;
                    @endphp
                    @if($tags)
                    <div class="post-tags">
                        <p class="tags-title">
                            Метки
                        </p>
                        <div class="tags-list">
                            @foreach($tags as $tag)
                            <a href="{{route('tag.show', $tag->slug)}}" class="post-tag">{{ $tag->title }}</a>
                            @endforeach
                        </div>
                    </div>
                    @endif
                    <div class="posts-nav">

                        @if($post->hasPrevious())

                        <div class="next-post post-nav-item">
                            <a href="{{ route('post.show', $post->getPrevious()->slug) }}" class="nav-arrows left" title="{{ $post->getPrevious()->title }}">
                                <img src="{{ asset('images/left-arrow.svg') }}" alt="">
                                <span>Предыдущая новость</span>
                            </a>
                            <div class="item">
                                <div class="small-post-thumb"
                                     style="background-image: url({{ $post->getPrevious()->getImage() }})">
                                    <a href="{{ route('post.show', $post->getPrevious()->slug) }}" title="{{ $post->getPrevious()->title }}"></a>
                                </div>
                                <a href="{{ route('post.show', $post->getPrevious()->slug) }}" class="small-post-info">
                                    <h6 class="cut-text">{{ $post->getPrevious()->title }}</h6>
                                    <p class="small-post-date">{{ $post->getPrevious()->date }}</p>
                                </a>
                            </div>
                        </div>

                        @endif

                        @if($post->hasNext())

                        <div class="prev-post post-nav-item">
                            <a href="{{ route('post.show', $post->getNext()->slug) }}" class="nav-arrows right" title="{{ $post->getNext()->title }}">
                                <span>Следующая новость</span>
                                <img src="{{ asset('images/right-arrow.svg') }}" alt="">
                            </a>
                            <div class="item">
                                <div class="small-post-thumb"
                                     style="background-image: url({{ $post->getNext()->getImage() }})">
                                    <a href="{{ route('post.show', $post->getNext()->slug) }}" title="{{ $post->getNext()->title }}"></a>
                                </div>
                                <a href="{{ route('post.show', $post->getNext()->slug) }}" class="small-post-info">
                                    <h6 class="cut-text">{{ $post->getNext()->title }}</h6>
                                    <p class="small-post-date">{{ $post->getNext()->date }}</p>
                                </a>
                            </div>
                        </div>

                        @endif

                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="link-images">
                        @forelse($post_ads['sidebar'] as $s_ad)
                            <a href="{{ $s_ad->link }}" target="_blank" class="mb-1">
                                <img src="{{ $s_ad->getImage() }}" class="img-fluid" alt="Превью">
                            </a>
                        @empty
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="content-bottom pt-5 pb-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-7 col-12">

                </div>
                <div class="col-md-5 col-12">
                    <div class="popular-tags">
                        <p class="tags-title">
                            Популярные метки
                        </p>
                        <div class="tags-list">
                            <a href="#" class="post-tag">Коронавирус</a>
                            <a href="#" class="post-tag">Хайфа новости</a>
                            <a href="#" class="post-tag">ЛГБТ</a>
                            <a href="#" class="post-tag">Хайфа новости</a>
                            <a href="#" class="post-tag">Хайфа новости</a>
                            <a href="#" class="post-tag">Хайфа новости</a>
                            <a href="#" class="post-tag">Хайфа новости</a>
                            <a href="#" class="post-tag">Хайфа новости</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="latest-posts-inner">
        <div class="container-fluid">
            <div class="row">
                <div class="col-4 mb-4">
                    <h4 class="post-latest-title pb-2">Последние статьи</h4>
                </div>
                <div class="col-12">
                    <div class="article-list popular">
                        @foreach($latest_posts as $post_small)
                            @include('pages.partials.small_post', $post_small)
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
