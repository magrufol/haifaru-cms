@extends('layouts.site')

@section('content')
    <section class="category-section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-9">
                    <h2 class="section-title">
                        <a href="#" style="background-color: #82B3E2;">{{ $title }}</a>
                    </h2>

                    <div class="article-list">
                        @foreach($posts as $post_small)
                            @include('pages.partials.small_post', $post_small)
                        @endforeach
                    </div>
                    <div class="pagination d-flex justify-content-center">
                        {{ $posts->links() }}
                    </div>

                </div>
                <div class="col-md-3 rb">
                    333
                </div>
            </div>
        </div>
    </section>
@endsection
