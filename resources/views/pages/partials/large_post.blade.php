<article class="hero-article">
    <div class="article-image" style="background-image: url({{ $post_ext->getImage() }})">
        <div class="overlay"></div>
        <span class="article-date">{{ $post_ext->date }}</span>
    </div>
    <div class="article-info">
        <div class="article-category">
            <a href="" class="category-tag blue-tag">{{ $post_ext->getCategoryTitle() }}</a>
        </div>
        <h2 class="article-title">
            {{ $post_ext->title }}
        </h2>
        <a href="{{ route('post.show', $post_ext->slug) }}" class="article-link">Читать полностью</a>
    </div>
</article>
