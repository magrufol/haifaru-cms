<article class="article-preview-item">
    <div class="article-thumb" style="background-image: url({{ $post_small->getImage() }})">
        <a href="{{ route('post.show', $post_small->slug) }}" title="{{ $post_small->title }}"></a>
    </div>
    <div class="article-info d-flex flex-column justify-content-between">
        <div>
            <h4 class="article-title">
                <a href="{{ route('post.show', $post_small->slug) }}" title="{{ $post_small->title }}">
                    {{ $post_small->title }}
                </a>
            </h4>
            <p class="article-desc">
                {{ \Illuminate\Support\Str::limit(strip_tags($post_small->content), 100, $end='...') }}
            </p>
        </div>

        <div>
            <a href="{{ route('post.show', $post_small->slug) }}" class="article-link" title="{{ $post_small->title }}">
                Читать полностью
            </a>
        </div>
    </div>
</article>
