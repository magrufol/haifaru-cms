@extends('layouts.site')

@section('content')
    <section class="top-articles">
        <div class="container-fluid">
            <div class="row">
                @foreach($latest as $post_ext)
                    <div class="col-xl-4 col-md-4">
                        @include('pages.partials.large_post', $post_ext)
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <section class="category-section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="section-title">
                        <span style="background-color: #FF6E4E;">Популярные статьи</span>
                    </h2>

                    <div class="article-list popular">
                        <article class="article-preview-item">
                            <div class="article-thumb" style="background-image: url(https://picsum.photos/426/387)">
                                <a href="" title=""></a>
                            </div>
                            <div class="article-info d-flex flex-column justify-content-between">
                                <div>
                                    <h4 class="article-title">
                                        <a href="" title=" Мэрия объявила два конкурса на проведение Мэрия объявила два конкурса на проведение">
                                            Мэрия объявила два конкурса на проведение...
                                        </a>
                                    </h4>
                                    <p class="article-desc">
                                        Утром производственная
                                        авария произошла на предприятии,
                                        находящемся авария произошла на предприятии...
                                    </p>
                                </div>

                                <div>
                                    <a href="" class="article-link" title="">
                                        Читать полностью
                                    </a>
                                </div>
                            </div>
                        </article>
                        <article class="article-preview-item">
                            <div class="article-thumb" style="background-image: url(https://picsum.photos/426/387)">
                                <a href="" title=""></a>
                            </div>
                            <div class="article-info d-flex flex-column justify-content-between">
                                <div>
                                    <h4 class="article-title">
                                        <a href="" title=" Мэрия объявила два конкурса на проведение Мэрия объявила два конкурса на проведение">
                                            Мэрия объявила два конкурса на проведение...
                                        </a>
                                    </h4>
                                    <p class="article-desc">
                                        Утром производственная
                                        авария произошла на предприятии,
                                        находящемся авария произошла на предприятии...
                                    </p>
                                </div>

                                <div>
                                    <a href="" class="article-link" title="">
                                        Читать полностью
                                    </a>
                                </div>
                            </div>
                        </article>
                        <article class="article-preview-item">
                            <div class="article-thumb" style="background-image: url(https://picsum.photos/426/387)">
                                <a href="" title=""></a>
                            </div>
                            <div class="article-info d-flex flex-column justify-content-between">
                                <div>
                                    <h4 class="article-title">
                                        <a href="" title=" Мэрия объявила два конкурса на проведение Мэрия объявила два конкурса на проведение">
                                            Мэрия объявила два конкурса на проведение...
                                        </a>
                                    </h4>
                                    <p class="article-desc">
                                        Утром производственная
                                        авария произошла на предприятии,
                                        находящемся авария произошла на предприятии...
                                    </p>
                                </div>

                                <div>
                                    <a href="" class="article-link" title="">
                                        Читать полностью
                                    </a>
                                </div>
                            </div>
                        </article>
                    </div>

                </div>
            </div>
        </div>
    </section>

    @if($categories_section)
        @foreach($categories_section as $cat)
            <section class="category-section">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-9">
                            <h2 class="section-title">
                                <a href="#" style="background-color: #82B3E2;">{{ $cat['title'] }}</a>
                            </h2>

                            <div class="article-list">
                                @foreach($cat['posts'] as $post_small)
                                    @include('pages.partials.small_post', $post_small)
                                @endforeach
                            </div>

                        </div>
                        <div class="col-md-3 rb">
                            333
                        </div>
                    </div>
                </div>
            </section>
        @endforeach
    @endif
@endsection
