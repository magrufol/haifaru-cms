<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 3 | Log in</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="/css/admin/icheck-bootstrap.min.css">
    <link rel="stylesheet" href="/css/admin/admin.css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page">

@yield('content')

<script src="/js/admin/jquery.min.js"></script>
<script src="/js/admin/bootstrap.bundle.min.js"></script>
<script src="/js/admin/adminlte.min.js"></script>
</body>
</html>
