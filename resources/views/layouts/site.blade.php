<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#09009A">
    <meta name="msapplication-navbutton-color" content="#09009A">
    <meta name="apple-mobile-web-app-status-bar-style" content="#09009A">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;700&family=Ubuntu:wght@300;400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/front/app.css') }}">
    <title>HaifaRu</title>
    <link rel="preload" as="image" href="https://picsum.photos/426/387">
</head>
<body class="{{ $view_classes }}">
<header id="site-header">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <div class="col-md-auto d-flex align-items-center">
                <nav class="main-menu">
                    <ul>
                        <li class="{{ Request::is('/') ? 'current-page' : '' }}">
                            <a href="/" title="Главная">Главная</a>
                        </li>
                        <li class="{{ Request::is('category/news') ? 'current-page' : '' }}">
                            <a href="category/news" title="Новости">Новости</a>
                        </li>
                        <li class="{{ Request::is('category/history') ? 'current-page' : '' }}">
                            <a href="category/history" title="История">История</a>
                        </li>
                        <li class="{{ Request::is('category/rest') ? 'current-page' : '' }}">
                            <a href="category/rest" title="Афиша">Афиша</a>
                        </li>
                        <li class="{{ Request::is('category/tours') ? 'current-page' : '' }}">
                            <a href="category/tours" title="Экскурсии">Экскурсии</a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="col-md-auto">
                <a href="/">
                    <img src="{{ asset('images/logo_full.png') }}" alt="">
                </a>
            </div>
        </div>
    </div>
</header>

@yield('content')


<footer>
    <div class="container-fluid px-4 mb-5">
        <div class="row justify-content-between">
            <div class="col-auto">
                <a href="https://www.facebook.com/groups/228377432160" target="_blank" class="fb-group-link d-flex align-items-center">
                    <img src="{{ asset('images/logo_full.png') }}" alt="Haifaru.co.il" title="Новости Хайфы - haifaru.co.il">
                    <i class="fab fa-facebook-square"></i>
                </a>
            </div>

            <div class="col-md-6">
                <h6 class="newsletter-title">
                    Хотите получить полезную информацию об интересных мероприятиях, проводимых в Хайфе? <br> Наша многолетняя интернет-рассылка вам о них расскажет.
                </h6>
                <form action="" id="newsletter-form" class="newsletter">
                    <input type="email" name="newsletter_mail" id="newsletter_mail" required placeholder="Введите Ваш Email">
                    @csrf
                    <button type="submit" id="newsletter-submit">Подписаться</button>
                    <span class="form-msg"></span>
                </form>
            </div>
        </div>
    </div>
    <div class="container-fluid px-4">
        <div class="row justify-content-between">
            <div class="col-md-4">
                <h5 class="footer-title">Категории</h5>
                @if($categories)
                    <ul class="category-list">
                        @foreach($categories as $cat)
                            @if($cat->postsCount())
                                <li>
                                    <a href="#">{{ $cat->title }}</a>
                                    <span>{{ $cat->postsCount() }}</span>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                @endif
                <div class="d-flex justify-content-between">
                    <a href="/" class="footer-logo">
                        <img src="{{ asset('images/logo_full.png') }}" alt="логотип haifaru.co.il" title="Новости Хайфы - haifaru.co.il">
                    </a>
                    <a href="" class="facebook-link" title="Наша группа в Facebook">
                        <img src="{{ asset('images/facebook.svg') }}" alt="facebook logo" title="Группа haifaru в Фейсбук">
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <h5 class="footer-title">Календарь новостей</h5>
                <div id="posts-calendar"></div>
            </div>
            <div class="col-md-4">
                <h5 class="footer-title">Популярные новости</h5>
                <div class="popular-posts post-nav-item flex-column">
                    <div class="post-nav-item mb-2">
                        <div class="item">
                            <div class="small-post-thumb"
                                 style="background-image: url(https://picsum.photos/426/387)">
                                <a href="#" title=""></a>
                            </div>
                            <a href="" class="small-post-info">
                                <h6 class="cut-text">Мэрия объявила два конкурса на проведение и замещение на проведение и замещение на проведение и замещение</h6>
                                <p class="small-post-date">12/08/2019</p>
                            </a>
                        </div>
                    </div>
                    <div class="post-nav-item mb-2">
                        <div class="item">
                            <div class="small-post-thumb"
                                 style="background-image: url(https://picsum.photos/426/387)">
                                <a href="#" title=""></a>
                            </div>
                            <a href="" class="small-post-info">
                                <h6 class="cut-text">Мэрия объявила два конкурса на проведение и замещение на проведение и замещение на проведение и замещение</h6>
                                <p class="small-post-date">12/08/2019</p>
                            </a>
                        </div>
                    </div>
                    <div class="post-nav-item mb-2">
                        <div class="item">
                            <div class="small-post-thumb"
                                 style="background-image: url(https://picsum.photos/426/387)">
                                <a href="#" title=""></a>
                            </div>
                            <a href="" class="small-post-info">
                                <h6 class="cut-text">Мэрия объявила два конкурса на проведение и замещение на проведение и замещение на проведение и замещение</h6>
                                <p class="small-post-date">12/08/2019</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center copyright">
            <div class="col-md-auto col-12">
                <p>© 2017 Copyright by haifaru.co.il | Created by <a href="https://magru.co.il/" target="_blank" title="Magru WebDev - разработка интернет приложений">Magru
                        <img src="{{ asset('images/magru.png') }}" alt="magru logo" title="Magru WebDev - разработка интернет приложений"></a></p>
            </div>
        </div>
        <div class="row">
            <div class="col-12 d-flex justify-content-center">
                <div class="bottom-menu">
                    <ul>
                        <li>
                            <a href="">Главная</a>
                        </li>
                        <li>
                            <a href="">Новости</a>
                        </li>
                        <li>
                            <a href="">История</a>
                        </li>
                        <li>
                            <a href="">Афиша</a>
                        </li>
                        <li>
                            <a href="">Экскурсии</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

@if($show_share_bar)
<div class="float-share-links d-flex justify-content-between align-items-center">
    <div class="container-fluid h-100">
        <div class="row h-100">
            <div class="col-md-4 col-3 d-flex align-items-center">
                <div class="share-block d-flex">
                    <p class="share-title">Поделиться этой новостью</p>
                    <div class="d-flex align-items-center">
                        <a href="https://www.facebook.com/sharer/sharer.php?u={{ Request::url() }}" class="mx-1" title="Поделиться в Facebook" target="_blank">
                            <img src="{{ asset('images/facebook-share.png') }}" alt="Facebook" title="Поделиться в Facebook">
                        </a>
                        <a href="http://twitter.com/share?text=&url={{ Request::url() }}&hashtags=haifaru.co.il,НовостиХайфы" class="mx-1" title="Поделиться в Twitter" target="_blank">
                            <img src="{{ asset('images/twitter-share.png') }}" alt="Twitter" title="Поделиться в Twitter">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 d-md-flex d-none align-items-center justify-content-center">
                <div class="fb-group-link group-post-link">
                    <a href="{{ $fb_post_link }}" target="_blank" title="Обсудить новость в группе">
                        <span>Обсудить новость в группе</span>
                        <img src="{{ asset('images/fb-white.png') }}" alt="Facebook logo" title="Обсудить новость в группе">
                    </a>
                </div>
            </div>
            <div class="col-md-4 col-9 d-flex align-items-center justify-content-end">
                <div class="contact-us-link">
                    <a href="" class="btn--shockwave" title="Разместить рекламу на сайте и группе">
                        <span class="small">Хотите разместить свою рекламу? </span>
                        <span class="large">Свяжитесь с нами</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

<script src="{{ asset('js/front/app.js') }}"></script>
</body>
</html>
