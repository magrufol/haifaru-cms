@extends('layouts.admin')

@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Создание рекламного блока</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">General Form</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content">
            {!! Form::open(['route' => ['ads.store'], 'files' => true]) !!}
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        @include('admin.errors')
                    </div>
                    <div class="col-md-8">
                        <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-body row">
                                <div class="form-group col-6">
                                    <label for="title">Название</label>
                                    <input type="text" name="title" class="form-control form-control-lg" id="title"
                                           value="">
                                </div>
                                <div class="form-group col-6">
                                    <label for="link">Ссылка</label>
                                    <input type="text" name="link" class="form-control form-control-lg" id="link"
                                           value="">
                                </div>
                                <div class="form-group col-6">
                                    <label for="exampleInputFile">Лицевая картинка</label>
                                    <div class="card" id="holder" style="width: 18rem;">

                                    </div>
                                    {{--                                            <input type="file" name="image" id="postImageInput"--}}
                                    {{--                                                   onchange="preview_image(event)">--}}


                                    <div class="input-group">
                                                <span class="input-group-btn">
                                                <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary text-white">
                                                    <i class="fa fa-picture-o"></i> Выбрать
                                                </a>
                                                </span>
                                        <input id="thumbnail" class="form-control" type="text" name="image">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label for="note">Описание</label>
                                    <textarea type="text" name="note" class="form-control form-control-lg" id="note">
                                    </textarea>
                                </div>
                            </div>

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </section>
    </div>
@endsection
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script>
    var route_prefix = "/filemanager";
    {!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/stand-alone-button.js')) !!}

    var lfm = function(id, type, options) {
        let button = document.getElementById('lfm');

        button.addEventListener('click', function () {
            var route_prefix = (options && options.prefix) ? options.prefix : '/filemanager';
            var target_input = document.getElementById(button.getAttribute('data-input'));
            var target_preview = document.getElementById(button.getAttribute('data-preview'));

            window.open(route_prefix + '?type=' + type || 'file', 'FileManager', 'width=900,height=600');
            window.SetUrl = function (items) {
                var file_path = items.map(function (item) {
                    return item.name;
                }).join(',');

                // set the value of the desired input to image url
                target_input.value = file_path;
                target_input.dispatchEvent(new Event('change'));


                // clear previous preview
                target_preview.innerHtml = '';

                // set or change the preview image src
                items.forEach(function (item) {
                    let img = document.createElement('img')
                    //img.setAttribute('style', 'height: 5rem')
                    img.classList.add("img-responsive");
                    img.classList.add("img-thumbnail");
                    img.setAttribute('src', item.thumb_url)
                    target_preview.appendChild(img);
                });

                // trigger change event
                target_preview.dispatchEvent(new Event('change'));
            };
        });
    };

    $( document ).ready(function() {
        lfm('lfm', 'image', {prefix: route_prefix});
    });

</script>
