@extends('layouts.admin')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>DataTables</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Список публикаций</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <table id="posts-list" data-order='[[ 7, "asc" ]]' data-page-length='30' class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Картинка</th>
                                        <th>Заголовок</th>
                                        <th>Категория</th>
                                        <th>Теги</th>
                                        <th>Статус</th>
                                        <th>Дата создания</th>
                                        <th>Действия</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($posts as $post)
                                        <tr>
                                            <td>{{ $post->id }}</td>
                                            <td>
                                                <img src="{{ $post->getImage(true) }}" width="100px" alt="{{ $post->title }}" class="img-thumbnail">
                                            </td>
                                            <td>
                                                {{ $post->title }}
                                                <br>
                                                <a href="{{ URL::to('/') . '/' . $post->slug }}" target="_blank">
                                                    <span>Ссылка на пост</span>
                                                    <i class="fas fa-external-link-alt"></i>
                                                </a>
                                            </td>
                                            <td>{{ $post->getCategoryTitle() }}</td>
                                            <td>{{ $post->getTagTitles() }}</td>
                                            <td>
                                                <small class="badge {{ $post->status ? 'badge-success' : 'badge-danger' }} ">
                                                    {{ $post->status ? 'Онлайн' : 'Черновик' }}
                                                </small>
                                            </td>
                                            <td>{{ $post->created_at->format('d.m.Y G:i') }}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-flat btn-info">
                                                        <i class="fas fa-edit"></i>
                                                    </a>

                                                    {{ Form::open(['route' => ['posts.destroy', $post->id], 'method'=>'delete']) }}
                                                    <button onclick="return confirm('Вы уверены ?')" type="submit" class="btn btn-flat btn-danger">
                                                        <i class="fas fa-trash-alt"></i>
                                                    </button>
                                                    {{ Form::close() }}
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>ID</th>
                                        <th>Картинка</th>
                                        <th>Заголовок</th>
                                        <th>Категория</th>
                                        <th>Теги</th>
                                        <th>Статус</th>
                                        <th>Дата создания</th>
                                        <th>Действия</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
