@extends('layouts.admin')

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Создание новой публикации</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">General Form</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content">
            {{ Form::open([
                'route' => ['posts.store'],
                 'files' => true,
              ])
            }}
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        @include('admin.errors')
                    </div>
                    <div class="col-md-8">
                        <div class="card card-primary">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Заголовок</label>
                                    <input type="text" name="title" class="form-control form-control-lg" id="title"
                                           value="">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Полный текст</label>
                                    <textarea name="content" id="content" cols="30" rows="10"
                                              class="form-control"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Ссылка на пост в Facebook</label>
                                    <input type="text" name="fb_post_link" class="form-control form-control-lg"
                                           id="fb_post_link"
                                           value="">
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                    <div class="col-md-4">
                        <div class="card card-warning">
                            <div class="card-header">
                                <h3 class="card-title">Настройки публикации</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">

                                        <div class="form-group">
                                            <label for="exampleInputFile">Активная публикация</label>
                                            <div class="input-group">
                                                <input type="checkbox" name="status" data-bootstrap-switch
                                                       data-off-color="danger" data-on-color="success">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Категория</label>
                                            {{Form::select('category_id',
                                                $categories,
                                                null,
                                                ['class' => 'form-control select2bs4']) }}
                                        </div>

                                        <div class="form-group">
                                            <label>Теги</label>

                                            {{Form::select('tags[]',
                                                $tags,
                                                null,
                                                ['class' => 'form-control select2bs4', 'multiple' => 'multiple', 'data-placeholder' => 'Выберите теги']) }}

                                        </div>
                                        <div class="form-group">
                                            <label>Дата:</label>
                                            <div class="input-group date" id="postdate"
                                                 data-value="" data-target-input="nearest">
                                                <input type="text" class="form-control datetimepicker-input" name="date"
                                                       data-target="#postdate"/>
                                                <div class="input-group-append" data-target="#postdate"
                                                     data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputFile">Лицевая картинка</label>
                                            <div class="card" id="holder" style="width: 18rem;">

                                            </div>
                                            {{--                                            <input type="file" name="image" id="postImageInput"--}}
                                            {{--                                                   onchange="preview_image(event)">--}}


                                            <div class="input-group">
                                                <span class="input-group-btn">
                                                <a id="lfm" data-input="thumbnail" data-preview="holder"
                                                   class="btn btn-primary text-white">
                                                    <i class="fa fa-picture-o"></i> Выбрать
                                                </a>
                                                </span>
                                                <input id="thumbnail" class="form-control" type="text" name="image">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card card-warning">
                            <div class="card-header">
                                <h3 class="card-title">Рекламные блоки</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table id="ads_post" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Картинка</th>
                                                <th>Заголовок</th>
                                                <th>Ссылка</th>
                                                <th>Действия</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($ads as $ad)
                                                <tr>
                                                    <td>{{ $ad->id }}</td>
                                                    <td>
                                                        <img src="{{ $ad->getImage(true) }}" width="200px"
                                                             alt="{{ $ad->title }}" class="img-thumbnail">
                                                    </td>
                                                    <td>{{ $ad->title }}</td>
                                                    <td>
                                                        <a href="{{ $ad->link }}">Ссылка</a>
                                                    </td>
                                                    <td>
                                                        <div class="icheck-success d-inline for-{{ $ad->id }}">
                                                            <input type="checkbox"  name="post_ads_content[]"
                                                                   value="{{ $ad->id }}" class="ad-type"  data-id="ad-{{ $ad->id }}-right" id="ad-{{ $ad->id }}-content">
                                                            <label for="ad-{{ $ad->id }}-content">
                                                                После контента
                                                            </label>
                                                        </div>
                                                        <br>
                                                        <br>
                                                        <div class="icheck-success d-inline for-{{ $ad->id }}">
                                                            <input type="checkbox"  name="post_ads_right[]"
                                                                   value="{{ $ad->id }}" class="ad-type" data-id="ad-{{ $ad->id }}-content" id="ad-{{ $ad->id }}-right">
                                                            <label for="ad-{{ $ad->id }}-right">
                                                                Справа
                                                            </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            {{ Form::close() }}
        </section>
    </div>
@endsection
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script>
    var route_prefix = "/filemanager";
    {!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/stand-alone-button.js')) !!}

    var lfm = function (id, type, options) {
        let button = document.getElementById('lfm');

        button.addEventListener('click', function () {
            var route_prefix = (options && options.prefix) ? options.prefix : '/filemanager';
            var target_input = document.getElementById(button.getAttribute('data-input'));
            var target_preview = document.getElementById(button.getAttribute('data-preview'));

            window.open(route_prefix + '?type=' + type || 'file', 'FileManager', 'width=900,height=600');
            window.SetUrl = function (items) {
                var file_path = items.map(function (item) {
                    return item.name;
                }).join(',');

                // set the value of the desired input to image url
                target_input.value = file_path;
                target_input.dispatchEvent(new Event('change'));

                // clear previous preview
                target_preview.innerHtml = '';

                // set or change the preview image src
                items.forEach(function (item) {
                    let img = document.createElement('img')
                    //img.setAttribute('style', 'height: 5rem')
                    img.classList.add("img-responsive");
                    img.classList.add("img-thumbnail");
                    img.setAttribute('src', item.thumb_url)
                    target_preview.appendChild(img);
                });

                // trigger change event
                target_preview.dispatchEvent(new Event('change'));
            };
        });
    };

    $(document).ready(function () {
        lfm('lfm', 'image', {prefix: route_prefix});
    });

</script>
