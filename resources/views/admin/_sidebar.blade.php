<div class="sidebar">
    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-book-open"></i>
                    <p>
                        Публикации
                        <i class="fas fa-angle-left right"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview" style="display: none;">
                    <li class="nav-item">
                        <a href="{{route('posts.create')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Новая публикация</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('posts.index')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Список публикаций</p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-folder"></i>
                    <p>
                        Категории
                        <i class="fas fa-angle-left right"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview" style="display: none;">
                    <li class="nav-item">
                        <a href="{{route('categories.create')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Новая категория</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('categories.index')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Список категорий</p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-folder"></i>
                    <p>
                        Теги
                        <i class="fas fa-angle-left right"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview" style="display: none;">
                    <li class="nav-item">
                        <a href="{{route('tags.create')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Новый тег</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('tags.index')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Список тегов</p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-folder"></i>
                    <p>
                        Рекламные блоки
                        <i class="fas fa-angle-left right"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview" style="display: none;">
                    <li class="nav-item">
                        <a href="{{route('ads.create')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Новый блок</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('ads.index')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Список блоков</p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item has-treeview">
                <a href="{{route('settings')}}" class="nav-link">
                    <i class="nav-icon fas fa-folder"></i>
                    <p>
                        Настройки
                        <i class="fas fa-angle-left right"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview" style="display: none;">
                    <li class="nav-item">
                        <a href="{{route('settings.home')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Главная страница</p>
                        </a>
                        <a href="{{route('settings.adv')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Рекламные блоки</p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="/filemanager?type=image" target="_blank" class="nav-link">
                    <i class="nav-icon fas fa-folder"></i>
                    <p>Файлы</p>
                </a>
            </li>
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
</div>

