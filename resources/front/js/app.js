$( document ).ready(function() {

    $('#posts-calendar').datepicker({
        inline: true,
        showOtherMonths: true,
        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
            'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        firstDay: 0,
        isRTL: false,
        prevText: '',
        nextText: '',
        navigationAsDateFormat: true,
        nextText: 'MM',
        prevText: 'MM',
        dateFormat: "yy-mm-dd",
        onSelect: function (date) {
            window.location = "/date/" + date.toString();
        }
    });

    $('#newsletter-form').on('submit', function (e){
        e.preventDefault();
        var err = $('#newsletter-form .form-msg')
        err.removeClass('show-error');

        var email = $('#newsletter_mail').val();
        if(isEmail(email)) {
            var _crfm_token = $('#newsletter-form input[name="_token"]').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': _crfm_token
                }
            });
            jQuery.ajax({
                url: 'newsletter/subscribe_request',
                method: 'post',
                data: {
                    email: email
                },
                success: function (result) {
                    if(result.status === 'success'){
                        err.addClass('success');
                    }else{
                        err.addClass('error');
                    }

                    err.text(result.msg)
                }
            });
        }else{
            err.text('Нам нужна ваш электронная почта :)').addClass('error');
        }

    })

    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    window.onscroll = function() {
        stickyHeader()
    };

    var header = document.getElementById("site-header");
    var sticky = header.offsetTop;

    function stickyHeader() {
        if (window.pageYOffset > sticky) {
            header.classList.add("sticky");
        } else {
            header.classList.remove("sticky");
        }
    }

    $('.cut-text').text(function () {
        return  $(this).text().split(" ").splice(0,15).join(" ") + '...';
    })

    function truncate(input) {
        if (input.length > 20) {
            return input.substring(0, 20) + '...';
        }
        return input;
    };

    $('.hamburger').click(function(){
        $(this).toggleClass('is-active');
        $('.main-menu-col').toggleClass('menu-is-active')
    });

});
